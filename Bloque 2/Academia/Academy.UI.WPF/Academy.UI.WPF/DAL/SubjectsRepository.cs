﻿using System;
using System.Collections.Generic;
using System.Linq;
using Academy.Lib.Models;

namespace Academy.Lib.DAL
{
    class SubjectsRepository
    {
        private static Dictionary<Guid, Subject> Subjects
        {
            get
            {
                if (_subjects == null)
                {
                    _subjects = new Dictionary<Guid, Subject>();
                    var sub1 = new Subject()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Matematicas",
                        Code = "Mat001"
                    };
                    var sub2 = new Subject()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Biologia",
                        Code = "Bio001"
                    };

                    _subjects.Add(sub1.Id, sub1);
                    _subjects.Add(sub2.Id, sub2);
                }

                return _subjects;
            }
        }
        static Dictionary<Guid, Subject> _subjects;

        public static List<Subject> SubjectsList
        {
            get
            {
                return GetAll();
            }
        }

        public static List<Subject> GetAll()
        {
            return Subjects.Values.ToList();
        }

        public static Subject Get(Guid id)
        {

            if (Subjects.ContainsKey(id))
                return Subjects[id];
            else

                return default(Subject);
        }

        public static Subject GetByCode(string code)
        {
            foreach (var item in Subjects)
            {
                var subject = item.Value;
                if (subject.Code == code)
                    return subject;
            }

            return default(Subject);
        }

        public static List<Subject> GetByName(string name)
        {
            var output = new List<Subject>();

            foreach (var item in Subjects)
            {
                var subject = item.Value;

                if (subject.Name == name)
                    output.Add(subject);
            }

            return output;
        }

        public static SubjectValidationsTypes Add(Subject subject)
        {
            if (subject.Id != default(Guid))
            {
                return SubjectValidationsTypes.IdNotEmpty;
            }
            else if (!Subject.ValidateCodeFormat(subject.Code))
            {
                return SubjectValidationsTypes.WrongCodeFormat;
            }
            else
            {
                var subWithSameCode = GetByCode(subject.Code);
                if (subWithSameCode != null && subject.Id != subWithSameCode.Id)
                {
                    return SubjectValidationsTypes.CodeDuplicated;
                }
            }

            if (!Subject.ValidateName(subject.Name))
            {
                return SubjectValidationsTypes.WrongNameFormat;
            }
            else if (!Subjects.ContainsKey(subject.Id))
            {
                subject.Id = Guid.NewGuid();
                Subjects.Add(subject.Id, subject);

                return SubjectValidationsTypes.Ok;
            }

            return SubjectValidationsTypes.IdDuplicated;
        }

        public static SubjectValidationsTypes Update(Subject subject)
        {
            if (subject.Id == default(Guid))
            {
                return SubjectValidationsTypes.IdEmpty;
            }
            if (!Subjects.ContainsKey(subject.Id))
            {
                return SubjectValidationsTypes.SubjectNotFound;
            }

            if (!Subject.ValidateCodeFormat(subject.Code))
            {
                return SubjectValidationsTypes.WrongCodeFormat;
            }

            var subWithSameCode = GetByCode(subject.Code);
            if (subWithSameCode != null && subject.Id != subWithSameCode.Id)
            {
                return SubjectValidationsTypes.CodeDuplicated;
            }

            if (!Subject.ValidateName(subject.Name))
            {
                return SubjectValidationsTypes.WrongNameFormat;
            }

            Subjects[subject.Id] = subject;

            return SubjectValidationsTypes.Ok;
        }

        public static SubjectValidationsTypes Delete(Guid id)
        {
            if (Subjects.ContainsKey(id))
            {
                Subjects.Remove(id);
                return SubjectValidationsTypes.SubjectNotFound;
            }
            else
                return SubjectValidationsTypes.Ok;
        }
    }
}
