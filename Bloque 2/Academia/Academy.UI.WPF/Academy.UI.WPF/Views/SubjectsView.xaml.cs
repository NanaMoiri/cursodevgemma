﻿using Academy.Lib.DAL;
using Academy.Lib.Models;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;


namespace Academy.Views
{
    /// <summary>
    /// Interaction logic for SubjectsView.xaml
    /// </summary>
    public partial class SubjectsView : UserControl
    {
        public Subject SelectedSubject
        {
            get
            {
                return _selectedSubject;
            }
            set
            {
                _selectedSubject = value;
                if (value == null)
                {
                    BtSave.Visibility = Visibility.Hidden;

                    TbCode.Text = string.Empty;
                    TbName.Text = string.Empty;
                }
                else
                {
                    BtSave.Visibility = Visibility.Visible;

                    TbCode.Text = value.Code;
                    TbName.Text = value.Name;
                }
            }
        }
        Subject _selectedSubject;
        public SubjectsView()
        {
            InitializeComponent();

            DgSubjects.ItemsSource = SubjectsRepository.GetAll();


            var dict = new Dictionary<Guid, Subject>();
            var list = new List<Subject>();

            foreach (var item in dict)
            {
                var key = item.Key;
                var subject = item.Value;
                Console.WriteLine(subject.Name);
            }

            foreach (var subject in list)
            {

                Console.WriteLine(subject.Name);
            }

        }

        public void Clear()
        {
            SelectedSubject = null;
        }

        public void Create()
        {
            var subject = new Subject
            {
                Name = TbName.Text,
                Code = TbCode.Text
            };

            var addResult = SubjectsRepository.Add(subject);
            if (addResult != SubjectValidationsTypes.Ok)
                MessageBox.Show($"error adding subject:{addResult}");
            else
                DgSubjects.ItemsSource = SubjectsRepository.GetAll();

            Clear();
        }

        public void Retrieve()
        {
            var allSubjects = SubjectsRepository.GetAll();

            var idAleatorio = Guid.NewGuid();
            var subject = SubjectsRepository.Get(idAleatorio);

            var subject2 = SubjectsRepository.GetByCode("Mat001");
        }

        public void Update()
        {
            if (SelectedSubject != null)
            {
                var subjectCopy = SelectedSubject.Clone();

                subjectCopy.Code = TbCode.Text;
                subjectCopy.Name = TbName.Text;

                var editResult = SubjectsRepository.Update(subjectCopy);
                if (editResult != SubjectValidationsTypes.Ok)
                    MessageBox.Show($"error editing subject:{editResult}");
                else
                    DgSubjects.ItemsSource = SubjectsRepository.GetAll();
            }
        }

        public void Delete(Subject subject)
        {
            if (subject != null)
            {
                var deleteResult = SubjectsRepository.Delete(subject.Id);
                if (deleteResult != SubjectValidationsTypes.Ok)
                    MessageBox.Show($"error deleting subject:{deleteResult}");
                else
                    DgSubjects.ItemsSource = SubjectsRepository.GetAll();
            }
        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            Create();
        }

        private void BtSelect_Click(object sender, RoutedEventArgs e)
        {
            SelectedSubject = ((Button)sender).DataContext as Subject;
        }

        private void BtClear_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void BtSave_Click(object sender, RoutedEventArgs e)
        {
            Update();
            SelectedSubject = null;
        }

        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            var selectedSubject = ((Button)sender).DataContext as Subject;
            Delete(selectedSubject);
        }
    }
}