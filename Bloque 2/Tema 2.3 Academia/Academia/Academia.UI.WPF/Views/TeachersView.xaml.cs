﻿using EjemploCrud.Lib.DAL;
using EjemploCrud.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EjemploCrud.Views
{
    /// <summary>
    /// Interaction logic for TeachersView.xaml
    /// </summary>
    public partial class TeachersView : UserControl
    {
        public Teacher SelectedTeacher
        {
            get
            {
                return _selectedTeacher;
            }
            set
            {
                _selectedTeacher = value;
                if (value == null)
                {
                    BtSave.Visibility = Visibility.Hidden;

                    TbDni.Text = string.Empty;
                    TbName.Text = string.Empty;
                    TbEmail.Text = string.Empty;
                }
                else
                {
                    BtSave.Visibility = Visibility.Visible;

                    TbDni.Text = value.Dni;
                    TbName.Text = value.Name;
                    TbEmail.Text = value.Email;
                }
            }
        }
        Teacher _selectedTeacher;
        public TeachersView()
        {
            InitializeComponent();

            DgTeachers.ItemsSource = TeachersRepository.GetAll();


            var dict = new Dictionary<Guid, Teacher>();
            var list = new List<Teacher>();

            foreach (var item in dict)
            {
                var key = item.Key;
                var teacher = item.Value;
                Console.WriteLine(teacher.Name);
            }

            foreach (var teacher in list)
            {

                Console.WriteLine(teacher.Name);
            }

        }

        public void Clear()
        {
            SelectedTeacher = null;
        }

        public void Create()
        {
            var teacher = new Teacher
            {
                Name = TbName.Text,
                Email = TbEmail.Text,
                Dni = TbDni.Text
            };

            var addResult = TeachersRepository.Add(teacher);
            if (addResult != TeacherValidationsTypes.Ok)
                MessageBox.Show($"error adding teacher:{addResult}");
            else
                DgTeachers.ItemsSource = TeachersRepository.GetAll();

            Clear();
        }

        public void Retrieve()
        {
            var allTeachers = TeachersRepository.GetAll();

            var idAleatorio = Guid.NewGuid();
            var teacher = TeachersRepository.Get(idAleatorio);

            var teacher2 = TeachersRepository.GetByDni("12345678a");
        }

        public void Update()
        {
            if (SelectedTeacher != null)
            {
                var teacherCopy = SelectedTeacher.Clone();

                teacherCopy.Dni = TbDni.Text;
                teacherCopy.Name = TbName.Text;
                teacherCopy.Email = TbEmail.Text;

                var editResult = TeachersRepository.Update(teacherCopy);
                if (editResult != TeacherValidationsTypes.Ok)
                    MessageBox.Show($"error editing teacher:{editResult}");
                else
                    DgTeachers.ItemsSource = TeachersRepository.GetAll();
            }
        }

        public void Delete(Teacher teacher)
        {
            if (teacher != null)
            {
                var deleteResult = TeachersRepository.Delete(teacher.Id);
                if (deleteResult != TeacherValidationsTypes.Ok)
                    MessageBox.Show($"error deleting teacher:{deleteResult}");
                else
                    DgTeachers.ItemsSource = TeachersRepository.GetAll();
            }
        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            Create();
        }

        private void BtSelect_Click(object sender, RoutedEventArgs e)
        {
            SelectedTeacher = ((Button)sender).DataContext as Teacher;
        }

        private void BtClear_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void BtSave_Click(object sender, RoutedEventArgs e)
        {
            Update();
            SelectedTeacher = null;
        }

        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            var selectedTeacher = ((Button)sender).DataContext as Teacher;
            Delete(selectedTeacher);
        }
    }
}

