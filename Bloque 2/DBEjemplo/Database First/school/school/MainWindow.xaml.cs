﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace school
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var repository = new SchoolDbContext();

            //comprobar que ha ido bien la conexion 
            //var students = repository.students.ToList();

            if (repository.Students.Count() == 0)
            {
                var student1 = new Student()
                {
                    Name = "Manolo Pérez",
                    Dni = "478956A",
                    Email = "m@g.com"
                };

                var subject1 = new Subject()
                {
                    Name = "Matematicas",
                    Code = "Ma001"
                };
                repository.Students.Add(student1);
                repository.Subject.Add(subject1);
                repository.SaveChanges();

                var enrollment1 = new Enrollment()
                {
                    studentsId = student1.Id,
                    subjectsId = subject1.Id,
                    date = DateTime.Now
                };
                repository.Enrollment.Add(enrollment1);
                repository.SaveChanges();

            }
            DgStudents.ItemsSource = repository.Students.ToList();
            DgSubjects.ItemsSource = repository.Subject.ToList();
            DgEnrollment.ItemsSource = repository.Enrollment.ToList();
        }
    }
}
