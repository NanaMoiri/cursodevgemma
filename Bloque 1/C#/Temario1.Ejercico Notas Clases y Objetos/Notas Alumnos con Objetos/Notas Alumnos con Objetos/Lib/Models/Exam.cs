﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notas_Alumnos_con_Objetos.Lib.Models
{
    public class Exam : Entity
    {
        public Subject Subject { get; set; }

        public Student Student { get; set; }

        public double Mark { get; set; }

    }
}
