﻿using System;
using System.Collections.Generic;

namespace Letras_Repetidas
{
    class Program
    {
        static void Main()
        {
            //FASE 1

            var nombre = new char[5];

            nombre[0] = 'G';
            nombre[1] = 'e';
            nombre[2] = 'm';
            nombre[3] = 'm';
            nombre[4] = 'a';

            for (int i = 0; i < nombre.Length; i++)
            {
                Console.WriteLine(nombre[i]);
            } 

            //FASE 2

            List<char> nomList = new List<char>();

              nomList.Add('G');
              nomList.Add('e');
              nomList.Add('m');
              nomList.Add('m');
              nomList.Add('a');

                foreach (var letra in nomList)
                {
                    Console.WriteLine(letra);

                    if (letra == 'a' || letra == 'e')
                    {
                        Console.WriteLine("VOCAL");
                    }
                    else
                    {
                        Console.WriteLine("CONSONANTE");
                    }
                }
            //FASE 3

            var diccionario = Dictionary<char, int>();

            diccionario.Add('G', 1);
            diccionario.Add('e', 1);
            diccionario.Add('m', 2);
            diccionario.Add('a', 1);

            Console.WriteLine(Dictionary);
            


        }

        private static object Dictionary<T1, T2>()
        {
            throw new NotImplementedException();
        }
    }

}
