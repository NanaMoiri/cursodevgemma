﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploCrud.Lib.Models
{
    public class Subject : Entity
    {
        #region Static Validations

        public static bool ValidateCodeFormat(string code)
        {
            if (string.IsNullOrEmpty(code))
                return false;
            else if (code.Length != 6)
                return false;
            else
                return true;
        }

        public static bool ValidateName(string name)
        {
            if (string.IsNullOrEmpty(name.Trim()))
            {
                return false;
            }
            return true;
        }

        #endregion

        public string Code { get; set; }
        public string Name { get; set; }

        public Subject()
        {
        }

        public Subject Clone()
        {
            var output = new Subject
            {
                Id = this.Id,
                Code = this.Code,
                Name = this.Name,
            };

            return output;
        }
    }

    public enum SubjectValidationsTypes
    {
        Ok,
        WrongCodeFormat,
        CodeDuplicated,
        WrongNameFormat,
        IdNotEmpty,
        IdDuplicated,
        IdEmpty,
        SubjectNotFound
    }
}
