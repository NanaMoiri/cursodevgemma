﻿using EjemploCrud.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;


namespace EjemploCrud.Lib.DAL
{
    class TeachersRepository
    {
        private static Dictionary<Guid, Teacher> Teachers
        {
            get
            {
                if (_teachers == null)
                {
                    _teachers = new Dictionary<Guid, Teacher>();
                    var tch1 = new Teacher()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Pepe",
                        Email = "p@p.com",
                        Dni = "12345678a"
                    };
                    var tch2 = new Teacher()
                    {
                        Id = Guid.NewGuid(),
                        Name = "Marta",
                        Email = "m@m.com",
                        Dni = "12345678b"
                    };

                    _teachers.Add(tch1.Id, tch1);
                    _teachers.Add(tch2.Id, tch2);
                }

                return _teachers;
            }
        }
        static Dictionary<Guid, Teacher> _teachers;

        public static List<Teacher> TeachersList
        {
            get
            {
                return GetAll();
            }
        }

        public static List<Teacher> GetAll()
        {
            return Teachers.Values.ToList();
        }

        public static Teacher Get(Guid id)
        {

            if (Teachers.ContainsKey(id))
                return Teachers[id];
            else

                return default(Teacher);
        }

        public static Teacher GetByDni(string dni)
        {
            foreach (var item in Teachers)
            {
                var student = item.Value;
                if (student.Dni == dni)
                    return student;
            }

            return default(Teacher);
        }

        public static List<Teacher> GetByName(string name)
        {
            var output = new List<Teacher>();

            foreach (var item in Teachers)
            {
                var teacher = item.Value;

                if (teacher.Name == name)
                    output.Add(teacher);
            }

            return output;
        }

        public static TeacherValidationsTypes Add(Teacher teacher)
        {
            if (teacher.Id != default(Guid))
            {
                return TeacherValidationsTypes.IdNotEmpty;
            }
            else if (!Teacher.ValidateDniFormat(teacher.Dni))
            {
                return TeacherValidationsTypes.WrongDniFormat;
            }
            else
            {
                var tchWithSameDni = GetByDni(teacher.Dni);
                if (tchWithSameDni != null && teacher.Id != tchWithSameDni.Id)
                {
                    return TeacherValidationsTypes.DniDuplicated;
                }
            }

            if (!Teacher.ValidateName(teacher.Name))
            {
                return TeacherValidationsTypes.WrongNameFormat;
            }
            else if (!Teachers.ContainsKey(teacher.Id))
            {
                teacher.Id = Guid.NewGuid();
                Teachers.Add(teacher.Id, teacher);

                return TeacherValidationsTypes.Ok;
            }

            return TeacherValidationsTypes.IdDuplicated;
        }

        public static TeacherValidationsTypes Update(Teacher teacher)
        {
            if (teacher.Id == default(Guid))
            {
                return TeacherValidationsTypes.IdEmpty;
            }
            if (!Teachers.ContainsKey(teacher.Id))
            {
                return TeacherValidationsTypes.TeacherNotFound;
            }

            if (!Teacher.ValidateDniFormat(teacher.Dni))
            {
                return TeacherValidationsTypes.WrongDniFormat;
            }

            var tchWithSameDni = GetByDni(teacher.Dni);
            if (tchWithSameDni != null && teacher.Id != tchWithSameDni.Id)
            {
                return TeacherValidationsTypes.DniDuplicated;
            }

            if (!Teacher.ValidateName(teacher.Name))
            {
                return TeacherValidationsTypes.WrongNameFormat;
            }

            Teachers[teacher.Id] = teacher;

            return TeacherValidationsTypes.Ok;
        }

        public static TeacherValidationsTypes Delete(Guid id)
        {
            if (Teachers.ContainsKey(id))
            {
                Teachers.Remove(id);
                return TeacherValidationsTypes.TeacherNotFound;
            }
            else
                return TeacherValidationsTypes.Ok;
        }
    }
}
