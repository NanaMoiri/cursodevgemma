﻿using Notas_Alumnos_con_Objetos.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Notas_Alumnos_con_Objetos
{
    class Program
    {
        public static Dictionary<String, Student> Students = new Dictionary<string, Student>();
        public static Dictionary<String, Subject> Subjects = new Dictionary<string, Subject>();
        public static Dictionary<Guid, Exam> Exams = new Dictionary<Guid, Exam>();

        static void Main(string[] args)
        {
            InitData();
            Console.WriteLine("Welcome to the Student's Management Program");
            Console.WriteLine("To enter the Student Management use option 'a'");
            Console.WriteLine("To enter the  Exams marks menu use option 'e'");
            var keepdoing = true;

            while (keepdoing)
            {
                var option = Console.ReadKey().KeyChar;

                if (option == 'a')
                {
                    ShowStudentsMenu();
                }
                else if (option == 'e')
                {
                    ShowMarksMenu();
                }
            }

            #region StudentsMenu
            static void ShowStudentsMenu()
            {
                Console.WriteLine();
                ShowStudentsMenuOptions();

                var keepdoing = true;
                while (keepdoing)
                {
                    var text = Console.ReadLine();

                    switch (text)
                    {
                        case "all":
                            ShowAllStudents();
                            break;
                        case "add":
                            AddNewStudent();
                            break;
                        case "del":
                            DeleteStudent();
                            break;
                        case "edit":
                            EditStudent();
                            break;
                        case "m":
                            keepdoing = false;
                            break;
                        default:
                            ShowStudentsMenu();
                            break;
                    }
                }
            }

            static void ShowStudentsMenuOptions()
            {
                Console.WriteLine("--Student's Menu--");

                Console.WriteLine("To show all students write all");
                Console.WriteLine("To add a new student write add");
                Console.WriteLine("To edit a student write edit");
                Console.WriteLine("To delete a student write del");
                Console.WriteLine("To go back to the Main Menu write m");
            }

            static void ShowAllStudents()
            {
                foreach (var student in Students.Values)
                {
                    Console.WriteLine($"{student.Dni} {student.Name}");
                }
            }

            static void AddNewStudent()
            {
                Console.WriteLine("First introduce the dni or write cancel to exit");

                var keepdoing = true;
                while (keepdoing)
                {
                    var dni = Console.ReadLine();

                    if (dni == "cancel")
                    {
                        break;
                    }
                    else if (Students.ContainsKey(dni))
                    {
                        Console.WriteLine($"The student with this dni already exists");
                    }
                    else
                    {
                        while (true)
                        {
                            Console.WriteLine("Now input the name o write cancel to exit");
                            var name = Console.ReadLine();

                            if (name == "cancel")
                            {
                                keepdoing = false;
                                break;
                            }
                            if (string.IsNullOrEmpty(name))
                            {
                                Console.WriteLine("Name field is Empty");
                            }
                            else
                            {
                                var student = new Student
                                {
                                    Id = Guid.NewGuid(),
                                    Dni = dni,
                                    Name = name
                                };
                                Students.Add(student.Dni, student);
                                keepdoing = false;
                                break;
                            }
                        }
                    }
                }

                ShowStudentsMenuOptions();
            }
            static void DeleteStudent()
            {
                Console.WriteLine("Student delete mode, please input student's DNI or write cancel to exit");
                var dni = Console.ReadLine();
                var keepdoing = true;

                while (keepdoing)
                {
                    if (dni == "cancel")
                    {
                        break;
                    }
                    else if (Students.ContainsKey(dni))
                    {
                        Students.Remove(dni);

                        Console.WriteLine("Student deleted successfuly");
                    }
                    else
                    {
                        Console.WriteLine("Student DNI not found, please input the right DNI");
                    }
                }

                ShowStudentsMenuOptions();
            }
            static void EditStudent()
            {
                Console.WriteLine("If you want to edit a student's DNI write dni or If you want to change student's name write name");
                Console.WriteLine("Write cancel to exit");
                var text = Console.ReadLine();
                switch (text)
                {
                    case "dni":
                        EditDni();
                        break;
                    case "name":
                        EditName();
                        break;
                    default:
                        break;
                }
            }
            static void EditName()
            {
                var keepdoing = true;
                while (keepdoing)
                {
                    Console.WriteLine("Input student's dni or cancel to exit");
                    var dni = Console.ReadLine();
                    if (dni == "cancel")
                    {
                        keepdoing = false;
                    }
                    else if (Students.ContainsKey(dni))
                    {
                        Console.WriteLine("Input new name");
                        var name = Console.ReadLine();
                        var student = Students[dni];
                        student.Name = name;
                        Console.WriteLine($"The name with of the student with dni {dni} has been changed to {name} successfuly");
                        break;
                    }
                }
                ShowStudentsMenuOptions();
            }
            static void EditDni()
            {
                var keepdoing = true;
                while (keepdoing)
                {
                    Console.WriteLine("Input student's dni or cancel to exit");
                    var dni = Console.ReadLine();
                    if (dni == "cancel")
                    {
                        keepdoing = false;
                    }
                    else if (Students.ContainsKey(dni))
                    {
                        Students.Remove(dni);
                        AddNewStudent();
                        break;
                    }
                }
            }
            #endregion
            static void ShowMarksMenu()
            {
                Console.WriteLine("--Subjects and Exams Menu--");
                Console.WriteLine("To get subject's list write 'subjects'");
                Console.WriteLine("To enter exams marks management write 'exams'");
                Console.WriteLine("To exit this menu write 'exit'");

                var keepdoing = true;
                while (keepdoing)
                {
                    var text = Console.ReadLine();

                    switch (text)
                    {
                        case "subjects":
                            ShowAllSubjects();
                            break;
                        case "exams":
                            ShowExamsMarksManagemetMenu();
                            break;
                        case "exit":
                            keepdoing = false;
                            break;
                    }
                }
            }
            static void ShowAllSubjects()
            {
                foreach (var subject in Subjects.Values)
                {
                    Console.WriteLine($"Subject {subject.Name} Teacher {subject.Teacher} ");
                }
            }
            static void ShowExamsMarksManagemetMenu()
            {
                Console.WriteLine("--Stundent's Marks Menu--");
                Console.WriteLine("To add a mark's student of an Exam write 'add'");
                Console.WriteLine("To edit a mark write 'edit'");

                var keepdoing = true;
                while (keepdoing)
                {
                    var text = Console.ReadLine();

                    switch (text)
                    {
                        case "add":
                            AddExamsMark();
                            break;
                        case "edit":
                            EditMark();
                            break;
                    }
                }
            }

            static void AddExamsMark()
            {
                Console.WriteLine("First introduce the dni of student or write cancel to exit");
                var keepdoing = true;
                while (keepdoing)
                {
                    var dni = Console.ReadLine();

                    if (dni == "cancel")
                    {
                        break;
                    }
                    else if (Students.ContainsKey(dni))
                    {
                        while (keepdoing)
                        {
                            Console.WriteLine("Now input the subject name o write 'cancel' to exit");
                            var subjName = Console.ReadLine();
                            if (subjName == "cancel")
                            {
                                keepdoing = false;
                                break;
                            }
                            else if (!Subjects.ContainsKey(subjName))
                            {
                                Console.WriteLine("Now input the mark o write 'cancel' to exit");
                                Console.ReadLine();
                                var mark = Double.Parse(Console.ReadLine());
                            }
                            else
                            {
                                var exam = new Exam()
                                {
                                    Id = Guid.NewGuid(),
                                    Subject = Subjects[subjName],
                                    Student = Students[dni],
                                    Mark = 0.0
                                };
                                Exams.Add(exam.Id, exam);
                                keepdoing = false;
                                break;
                            }

                        }
                    }
                }
            }


            static void EditMark()
            {
                Console.WriteLine("First introduce the dni of student or write cancel to exit");
                var keepdoing = true;
                while (keepdoing)
                {
                    var dni = Console.ReadLine();

                    if (dni == "cancel")
                    {
                        break;
                    }
                    else if (Students.ContainsKey(dni))
                    {
                        while (keepdoing)
                        {
                            Console.WriteLine("Now input the subject name o write 'cancel' to exit");
                            var subjName = Console.ReadLine();
                            if (subjName == "cancel")
                            {
                                keepdoing = false;
                                break;
                            }
                            else if (Subjects.ContainsKey(subjName))
                            {
                                Console.WriteLine("Now input the mark o write 'cancel' to exit");
                                var mark = Double.Parse(Console.ReadLine());
                                Console.ReadLine();
                                
                            }
                            else
                            {
                                var exam = new Exam()
                                {
                                    Id = Guid.NewGuid(),
                                    Subject = Subjects[subjName],
                                    Student = Students[dni],
                                    Mark = mark
                                };
                                Exams.Add(exam.Id, exam);
                                keepdoing = false;
                                break;
                            }

                        }
                    }
                }
            }

            public static void InitData()
            {
                var std1 = new Student()
                {
                    Id = Guid.NewGuid(),
                    Name = "Juana Pérez",
                    Dni = "123456Y"
                };
                var std2 = new Student()
                {
                    Id = Guid.NewGuid(),
                    Name = "Perico Lorenzo",
                    Dni = "235689T"
                };
                var std3 = new Student()
                {
                    Id = Guid.NewGuid(),
                    Name = "Toni López",
                    Dni = "789456P"
                };
                Students.Add(std1.Dni, std1);
                Students.Add(std2.Dni, std2);
                Students.Add(std3.Dni, std3);

                var subj1 = new Subject()
                {
                    Id = Guid.NewGuid(),
                    Name = "Math",
                    Teacher = "Eurelia"
                };
                var subj2 = new Subject()
                {
                    Id = Guid.NewGuid(),
                    Name = "History",
                    Teacher = "Eutaquio"
                };
                var subj3 = new Subject()
                {
                    Id = Guid.NewGuid(),
                    Name = "English",
                    Teacher = "Eugenio"
                };
                Subjects.Add(subj1.Name, subj1);
                Subjects.Add(subj2.Name, subj2);
                Subjects.Add(subj3.Name, subj3);

                var examMath = new Exam()
                {
                    Id = Guid.NewGuid(),
                    Subject = subj1,
                    Student = std1,
                    Mark = 9.0
                };
                var examMath2 = new Exam()
                {
                    Id = Guid.NewGuid(),
                    Subject = subj1,
                    Student = std2,
                    Mark = 7.5
                };
                var examMath3 = new Exam()
                {
                    Id = Guid.NewGuid(),
                    Subject = subj1,
                    Student = std3,
                    Mark = 9.0
                };

                var examHistory = new Exam()
                {
                    Id = Guid.NewGuid(),
                    Subject = subj2,
                    Student = std1,
                    Mark = 8.0
                };
                var examHistory2 = new Exam()
                {
                    Id = Guid.NewGuid(),
                    Subject = subj2,
                    Student = std2,
                    Mark = 5.0
                };
                var examHistory3 = new Exam()
                {
                    Id = Guid.NewGuid(),
                    Subject = subj2,
                    Student = std3,
                    Mark = 6.0
                };
                var examEnglish = new Exam()
                {
                    Id = Guid.NewGuid(),
                    Subject = subj3,
                    Student = std1,
                    Mark = 5.0
                };
                var examEnglish2 = new Exam()
                {
                    Id = Guid.NewGuid(),
                    Subject = subj3,
                    Student = std2,
                    Mark = 8.0
                };
                var examEnglish3 = new Exam()
                {
                    Id = Guid.NewGuid(),
                    Subject = subj3,
                    Student = std3,
                    Mark = 4.0
                };

            }
        }
    }
}


