﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ejercicio_Notas
{
    class Program
    {
        static string escapeword = "fin";
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenid@ al programa de gestión de notas");
            Console.WriteLine("Induzca las notas de los alumnos:");

            var notasAlumnos = new List<double>();
            bool keepgoing = true;

            while (keepgoing)
            {
                Console.WriteLine($"Nota Alumno {notasAlumnos.Count + 1}");
                var notaText = Console.ReadLine();

                if (notaText == escapeword)
                {
                    keepgoing = false;
                }
                else
                {
                    var nota = 0.0;

                    if (double.TryParse(notaText.Replace(".", ","), out nota))
                    {
                        notasAlumnos.Add(nota);
                    }
                    else
                    {
                        Console.WriteLine("La nota introducida no es correcta");
                    }
                }
            }

            var sum = 0.0;
            for (var i = 0; i < notasAlumnos.Count; i++)
            {
                sum += notasAlumnos[i];
            }

            var media = sum / notasAlumnos.Count;

            Console.WriteLine($"la media de las notas es {media}");

            Console.WriteLine("la nota más alta es: " + notasAlumnos.Max());
            Console.WriteLine("la nota mas baja es: " + notasAlumnos.Min());

            //while (keepgoing)
            //{
            //    Console.WriteLine("Para saber la nota más alta pulsa 1");
            //    Console.WriteLine("Para saber la nota más baja pulsa 2");
            //    var num = Console.ReadLine();

            //    if (num == escapeword)
            //    {
            //        keepgoing = false;
            //    }
            //    else
            //    {
            //        var nummax = 0.0;

            //        if (nummax == 1)
            //        {
            //            Console.WriteLine("La nota más alta es" + notasAlumnos.Max());
            //        }
            //        else
            //        {
            //            Console.WriteLine("La nota más baja es" + notasAlumnos.Min());
            //        }
            //    }

            //}
        }
    }
}
