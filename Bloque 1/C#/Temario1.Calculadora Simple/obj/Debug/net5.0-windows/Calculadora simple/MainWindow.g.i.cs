﻿#pragma checksum "..\..\..\..\Calculadora simple\MainWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3779A58399D3E77A7278E88F00F14C495AC13028"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Calculadora_simple;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Calculadora_simple {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 23 "..\..\..\..\Calculadora simple\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LbNum1;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\..\Calculadora simple\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbNum1;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\..\Calculadora simple\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LbNum2;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\..\Calculadora simple\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbNum2;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\..\Calculadora simple\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LbResult;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\..\Calculadora simple\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbResult;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\Calculadora simple\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Add;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\Calculadora simple\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Substract;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\..\Calculadora simple\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Multiply;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\Calculadora simple\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Divide;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "5.0.10.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Calculadora simple;V1.0.0.0;component/calculadora%20simple/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Calculadora simple\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "5.0.10.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LbNum1 = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.TbNum1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.LbNum2 = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.TbNum2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.LbResult = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.TbResult = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.Add = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\..\..\Calculadora simple\MainWindow.xaml"
            this.Add.Click += new System.Windows.RoutedEventHandler(this.Add_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.Substract = ((System.Windows.Controls.Button)(target));
            
            #line 38 "..\..\..\..\Calculadora simple\MainWindow.xaml"
            this.Substract.Click += new System.Windows.RoutedEventHandler(this.Substract_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.Multiply = ((System.Windows.Controls.Button)(target));
            
            #line 39 "..\..\..\..\Calculadora simple\MainWindow.xaml"
            this.Multiply.Click += new System.Windows.RoutedEventHandler(this.Multiply_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.Divide = ((System.Windows.Controls.Button)(target));
            
            #line 40 "..\..\..\..\Calculadora simple\MainWindow.xaml"
            this.Divide.Click += new System.Windows.RoutedEventHandler(this.Divide_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

