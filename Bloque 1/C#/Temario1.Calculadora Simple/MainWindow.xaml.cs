﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculadora_simple
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        public void Calculate(string op)
        {
            var num1 = Convert.ToDouble(TbNum1.Text);
            var num2 = Convert.ToDouble(TbNum2.Text);

            if (op == "+")
            {                
                double resultado = num1 + num2;
                var miresultado = Convert.ToString(resultado);
                TbResult.Text = miresultado;
            }
            else if (op == "-")
            {
                double resultado = num1 - num2;
                var miresultado = Convert.ToString(resultado);
                TbResult.Text = miresultado;
            }
            else if (op == "*")
            {
                double resultado = num1 * num2;
                var miresultado = Convert.ToString(resultado);
                TbResult.Text = miresultado;
            }
            else if (op == "/")
            {
                double resultado = num1 / num2;
                var miresultado = Convert.ToString(resultado);
                TbResult.Text = miresultado;
            }
                
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Calculate("+");
        }

        private void Substract_Click(object sender, RoutedEventArgs e)
        {
            Calculate("-");
        }

        private void Multiply_Click(object sender, RoutedEventArgs e)
        {
            Calculate("*");
        }

        private void Divide_Click(object sender, RoutedEventArgs e)
        {
            Calculate("/");
        }
    }
}
