﻿using CarDealer.Lib.Models;
using Microsoft.EntityFrameworkCore;

namespace CarDealer.Web.DAL
{
    public class CarsDealerDbContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }

        public CarsDealerDbContext() : base()
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Car>().ToTable("Cars");
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsbuilder)
        {
            optionsbuilder
                .UseMySql(connectionString: @"server=localhost;database=cars;uid=lolo;password=1234;",
                new MySqlServerVersion(new Version(8, 0, 23)));
        }

    }
}
