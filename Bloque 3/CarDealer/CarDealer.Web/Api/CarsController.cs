﻿using CarDealer.Lib.Models;
using CarDealer.Web.DAL;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CarDealer.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        // GET: api/<CarsController>
        [HttpGet]
        public IEnumerable<Car> Get()
        {
            var dbContext = new CarsDealerDbContext();

            if (dbContext.Cars.Count() == 0)
            {
                var car1 = new Car()
                {
                    Id = Guid.NewGuid(),
                    Brand = "Impala",
                    Door = 3,
                    License = "1234T",
                    Color = "Azúl Profundo"
                };
                var car2 = new Car()
                {
                    Id = Guid.NewGuid(),
                    Brand = "Gran Torino",
                    Door = 3,
                    License = "8523A",
                    Color = "Negro Azabache"
                };
                dbContext.Cars.Add(car1);
                dbContext.Cars.Add(car2);

                dbContext.SaveChanges();

            }
            return dbContext.Cars.ToList();
        }

        // GET api/<CarsController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<CarsController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<CarsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<CarsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
