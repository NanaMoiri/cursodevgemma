﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarDealer.Lib.Models
{
    public class Car
    {
        public Guid Id { get; set; }
        public string Brand { get; set; }
        public string License { get; set; }
        public string Color { get; set; }
        public int Door { get; set; }

    }

}
