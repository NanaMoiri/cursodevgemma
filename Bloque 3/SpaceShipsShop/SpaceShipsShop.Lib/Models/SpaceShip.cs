﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpaceShipsShop.Lib.Models
{
    public class SpaceShip
    {
        public Guid Id { get; set; }
        public string Brand { get; set; }
        public string Color { get; set; }
        public string Password { get; set; }
        public int FTLFactor { get; set; }
        public int PassengersCapacity { get; set; }

    }
}
