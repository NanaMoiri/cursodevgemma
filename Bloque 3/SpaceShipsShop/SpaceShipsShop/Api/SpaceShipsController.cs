﻿using Microsoft.AspNetCore.Mvc;
using SpaceShipsShop.DAL;
using SpaceShipsShop.Lib.Models;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SpaceShipsShop.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpaceShipsController : ControllerBase
    {
        // GET: api/<SpaceShipsController>
        [HttpGet]
        public IEnumerable<SpaceShip> Get()
        {
            var dbContext = new SpaceShipsShopDbContext();

            if(dbContext.SpaceShips.Count() == 0)
            {
                var ship1 = new SpaceShip()
                {
                    Id = Guid.NewGuid(),
                    Brand = "Pegasus",
                    FTLFactor = 10,
                    PassengersCapacity = 20,
                    Color = "Red",
                    Password = "Atenea.1234"
                };
                var ship2 = new SpaceShip()
                {
                    Id = Guid.NewGuid(),
                    Brand = "Phoenix",
                    FTLFactor = 8,
                    PassengersCapacity = 50,
                    Color = "Pink",
                    Password = "Cuca.1234"
                };
                dbContext.SpaceShips.Add(ship1);
                dbContext.SpaceShips.Add(ship2);

                dbContext.SaveChanges(); 
            }
            return dbContext.SpaceShips.ToList();

        }

        // GET api/<SpaceShipsController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<SpaceShipsController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<SpaceShipsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<SpaceShipsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
