﻿using Microsoft.EntityFrameworkCore;
using SpaceShipsShop.Lib.Models;

namespace SpaceShipsShop.DAL
{
    public class SpaceShipsShopDbContext : DbContext
    {
        public DbSet<SpaceShip> SpaceShips { get; set; }
        public SpaceShipsShopDbContext() : base()
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<SpaceShip>().ToTable("SpacesShips");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionbuilder)
        {
            optionbuilder
                .UseMySql(connectionString: @"server=localhost;database=sapceships;uid=lolo;password=1234;",
                new MySqlServerVersion(new Version(8, 0, 23)));
        }
    }
}
