class Spaceship
{
    Http = null;
    Spaceships = [];

    constructor($http)
    {
        this.Http = $http;
        this.GetAll();
        //this.Spaceships.push({Modelo:"Pegasus", FTL:40, Color:"Silver"});
        //this.Spaceships.push({Modelo:"Petra", FTL:20, Color:"Shine"});
    }
    GetAll()
    {
      this.Http(
        {
          method: 'GET',
          url: 'api/spaceships'
        })
        .then((respones) => this.LoadShips(response.data),
        function error(e)
        {
          alert(e.statusText);
        });
    }
    LoadShips(ships)
    {
      this.Spaceships = ships;
    }
}

App.
  component('spaceship', {
    templateUrl: 'scripts/views/spaceship/spaceship.html',
    controller: Spaceship,
    controllerAs: "vm"
  });
